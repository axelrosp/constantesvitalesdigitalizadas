package com.axelros.constantesvitalesdigitalizadas.exceptions

import java.lang.Exception

class NotValidDataException(message:String):Exception(message)
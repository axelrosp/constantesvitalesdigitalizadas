package com.axelros.constantesvitalesdigitalizadas.exceptions

import java.lang.Exception

class NotMeasuresException(message:String):Exception(message)
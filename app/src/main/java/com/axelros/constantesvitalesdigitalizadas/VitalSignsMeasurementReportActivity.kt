package com.axelros.constantesvitalesdigitalizadas

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import com.axelros.constantesvitalesdigitalizadas.controllers.vitalSignMeasurementReport.VitalSignMeasurementReportController
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.factory.controllersFactory.ControllersFactory
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.controllers.InactivityController.InactivityController
import kotlinx.android.synthetic.main.activity_vital_signs_measurement_report.*

class VitalSignsMeasurementReportActivity : AppCompatActivity() {

    private var vitalSignMeasurementReportController :VitalSignMeasurementReportController? = null
    private var customSharedPreferences: CustomSharedPreferences? = null
    private var inactivityController: InactivityController? = null


    private var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vital_signs_measurement_report)

        init()
    }

    private fun init() {
        customSharedPreferences = CustomSharedPreferences(this)
        vitalSignMeasurementReportController = ControllersFactory.createVitalSignsMeasurementReportController(this, applicationContext)
        inactivityController = InactivityController(customSharedPreferences!!, this, this)

        toolbar.title = getString(R.string.mesures_anteriors)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        initData()

        rvMeasurementsList?.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(this)
        rvMeasurementsList?.layoutManager = layoutManager

    }

    private fun initData() {
        vitalSignMeasurementReportController!!
            .getMeasurementReportOfPatient(intent.getStringExtra(Tags.PATIENT_ID.toString()))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_report_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        inactivityController!!.isInactive()
    }

    override fun onPause() {
        super.onPause()
        inactivityController!!.detectInactivityAndExecuteTimer()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


}

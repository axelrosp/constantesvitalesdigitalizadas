package com.axelros.constantesvitalesdigitalizadas.enums

enum class Tags {
    FLOOR,
    USER,
    LOGIN_USER_DTO,
    ACCES_TOKEN,
    REFRESH_TOKEN,
    PATIENT_NAME,
    PATIENT_ID,
    ROOM,
    PATIENT,
    PROFESSIONAL,
    VITAL_SIGNS_LIST,
    END_SESSION
}
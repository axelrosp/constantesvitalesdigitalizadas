package com.axelros.constantesvitalesdigitalizadas.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.interfaces.ClickListener
import com.axelros.constantesvitalesdigitalizadas.model.DTO.CensusDTO
import com.axelros.constantesvitalesdigitalizadas.utils.TimeCustomUtils

class CustomAdapterPatient(items:ArrayList<CensusDTO>, var clickListener: ClickListener): RecyclerView.Adapter<CustomAdapterPatient.CustomViewHolder>() {
    var items:ArrayList<CensusDTO>? = null
    var view: View? = null
    init {
        this.items = items
    }
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CustomViewHolder {
        view = LayoutInflater.from(parent.context).inflate(R.layout.item_patient, parent, false)
        return CustomViewHolder(view!!, clickListener)
    }

    override fun getItemCount(): Int {
        return items?.count()!!
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val item = items?.get(position)
        holder.tvTitlePatient?.text = item?.census?.cenNamePatient
        holder.tvRoomNumber?.text = item?.census?.cenNumRoom

        if(item?.msrDateHourEnd != null){
            holder.tvTitlePatient?.setTextColor(Color.WHITE)
            holder.llMeasured?.visibility = View.VISIBLE
            holder.llTitlePatient?.background = ContextCompat.getDrawable(view!!.context, R.drawable.layout_border_patient_consulted_item_title)
            holder.tvLastVisitHour?.text = TimeCustomUtils.convertTimeStampToHour(item.msrDateHourEnd.substringBefore("+"))
        }
        else{

            holder.tvTitlePatient?.setTextColor(Color.BLACK)
            holder.llMeasured?.visibility = View.GONE
            holder.llTitlePatient?.background = ContextCompat.getDrawable(view!!.context, R.drawable.layout_border_patient_item_title)
            holder.tvLastVisitHour?.text = ""
        }
    }

    class CustomViewHolder(view: View, clickListener:ClickListener):RecyclerView.ViewHolder(view), View.OnClickListener{

        var tvTitlePatient:TextView? = null
        var tvRoomNumber:TextView? = null
        var llMeasured:LinearLayout? = null
        var tvLastVisitHour:TextView? = null
        var llTitlePatient:LinearLayout? = null

        //Interface listener
        var clickListener:ClickListener? = null

        init {
            tvTitlePatient = view.findViewById(R.id.tvTitlePatient)
            tvRoomNumber = view.findViewById(R.id.tvRoomNumber)
            llMeasured = view.findViewById(R.id.llMeasured)
            tvLastVisitHour = view.findViewById(R.id.tvLastVisitHour)
            llTitlePatient = view.findViewById(R.id.llTitlePatient)
            this.clickListener = clickListener
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.clickListener?.onClick(v!!, adapterPosition)
        }


    }

}

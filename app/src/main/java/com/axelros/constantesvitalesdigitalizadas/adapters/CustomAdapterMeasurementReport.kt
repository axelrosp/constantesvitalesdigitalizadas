package com.axelros.constantesvitalesdigitalizadas.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.interfaces.ClickListener
import com.axelros.constantesvitalesdigitalizadas.model.DTO.VitalSignMeasurementDTO
import com.axelros.constantesvitalesdigitalizadas.utils.TimeCustomUtils

class CustomAdapterMeasurementReport(items:MutableCollection<MutableCollection<VitalSignMeasurementDTO>>,
                                     var clickListener: ClickListener):
    RecyclerView.Adapter<CustomAdapterMeasurementReport.CustomViewHolder>() {

    var items:MutableCollection<MutableCollection<VitalSignMeasurementDTO>>? = null
    var view: View? = null
    init {
        this.items = items
    }
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): CustomViewHolder {
        view = LayoutInflater.from(parent.context).inflate(R.layout.item_measurement, parent, false)
        return CustomViewHolder(view!!, clickListener)
    }

    override fun getItemCount(): Int {
        return items?.count()!!
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val item = items?.elementAt(position)
        val userRegistered = item?.elementAt(0)?.measurement?.userRegistered
        holder.tvProfesionalName?.text = "${userRegistered?.nombre}"
        holder.tvMeasurementDate?.text = TimeCustomUtils.getDateAndHourFromTimeStamp(
            item?.elementAt(0)?.measurement?.msrDateHourEnd!!.substringBefore("+")
        )
        holder.llVitalSignMeasurements?.removeAllViews()
        var countRow = 0
        item.forEach { vsm ->
            holder.llVitalSignMeasurements?.addView(addVitalSignMeasurementToReport(holder.itemView.context, vsm, countRow)
            )
            countRow++
        }
    }

    @SuppressLint("SetTextI18n")
    private fun addVitalSignMeasurementToReport(context: Context, vitalSignMeasurementDTO:VitalSignMeasurementDTO, countRow:Int):LinearLayout{
        val llWrapWrapParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)
        val llMatchWrapParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT)

        val llParent = LinearLayout(context)
        val color = getBackgroundColor(context, countRow)
        llParent.setBackgroundColor(color)
        llParent.layoutParams = llMatchWrapParams
        llParent.orientation = LinearLayout.HORIZONTAL

        val tvVitalSign = TextView(context)
        tvVitalSign.layoutParams = llWrapWrapParams
        tvVitalSign.text = vitalSignMeasurementDTO.vsmNemotechnical +": "+vitalSignMeasurementDTO.vsmValue
        tvVitalSign.textSize = 16F
        tvVitalSign.setTextColor(Color.BLACK)
        tvVitalSign.setPadding(16,8,0,8)

        val tvSystemTitle = TextView(context)
        if(vitalSignMeasurementDTO.vsmSystem.isNotEmpty()){
            tvSystemTitle.text = "Sistema: ${vitalSignMeasurementDTO.vsmSystem} Filtre: ${vitalSignMeasurementDTO.vsmFilter}"
        }

        tvSystemTitle.layoutParams = llMatchWrapParams
        tvSystemTitle.textSize = 14F
        tvSystemTitle.setPadding(0,8,16,8)
        tvSystemTitle.setTextColor(Color.BLACK)
        tvSystemTitle.textAlignment = View.TEXT_ALIGNMENT_TEXT_END

        llParent.addView(tvVitalSign)
        llParent.addView(tvSystemTitle)
        return llParent
    }

    private fun getBackgroundColor(context:Context, position:Int): Int{
        if(position%2 == 0) {
            return ContextCompat.getColor(context, R.color.backgroundCards)
        }
        return ContextCompat.getColor(context, R.color.backgroundCardsOds)
    }

    class CustomViewHolder(view: View, clickListener: ClickListener): RecyclerView.ViewHolder(view), View.OnClickListener{

        var tvProfesionalName: TextView? = null
        var tvMeasurementDate: TextView? = null
        var llVitalSignMeasurements: LinearLayout? = null

        //Interface listener
        var clickListener: ClickListener? = null

        init {
            tvProfesionalName = view.findViewById(R.id.tvProfesionalName)
            tvMeasurementDate = view.findViewById(R.id.tvMeasurementDate)
            llVitalSignMeasurements = view.findViewById(R.id.llVitalSignMeasurements)
            this.clickListener = clickListener
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            this.clickListener?.onClick(v!!, adapterPosition)
        }


    }

}

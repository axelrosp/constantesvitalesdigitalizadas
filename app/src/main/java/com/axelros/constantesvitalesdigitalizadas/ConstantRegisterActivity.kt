package com.axelros.constantesvitalesdigitalizadas

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Layout
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.axelros.constantesvitalesdigitalizadas.controllers.constantRegister.ConstantRegisterController
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.exceptions.NotMeasuresException
import com.axelros.constantesvitalesdigitalizadas.exceptions.NotValidDataException
import com.axelros.constantesvitalesdigitalizadas.factory.controllersFactory.ControllersFactory
import com.axelros.constantesvitalesdigitalizadas.factory.measurementsFactory.MeasurementsFactory
import com.axelros.constantesvitalesdigitalizadas.model.*
import com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper.VolleyErrorHelper
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.utils.CustomProgressDialog
import com.axelros.constantesvitalesdigitalizadas.controllers.InactivityController.InactivityController
import com.axelros.constantesvitalesdigitalizadas.utils.TimeCustomUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_constant_register.*
import kotlinx.android.synthetic.main.activity_constant_register.btnRegister
import kotlinx.android.synthetic.main.measure_dialog.*
import kotlinx.android.synthetic.main.measure_dialog.view.*
import java.util.*


class ConstantRegisterActivity : AppCompatActivity() {

    private var customSharedPreferences: CustomSharedPreferences? = null
    private var constantRegisterController : ConstantRegisterController? = null
    private var inactivityController: InactivityController? = null

    private var vitalSignsList: VitalSignsList? = null
    private var patient:CensusPatient? = null
    private var msrDateHourStart = ""

    private var measurementFields:MeasurementFields? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constant_register)

        init()
    }

    private fun init() {

        customSharedPreferences = CustomSharedPreferences(applicationContext)
        constantRegisterController = ControllersFactory.createConstantRegisterController(this, applicationContext)
        inactivityController = InactivityController(customSharedPreferences!!, this, this)

        //Get data when measurement activity is open
        msrDateHourStart = TimeCustomUtils.getCurrentTimeStamp

        //Hide keyboard when click
        clContainer?.setOnClickListener(::hideSoftKeyboard)

        //RegisterConstants
        btnRegister?.setOnClickListener {
            showRegisterDialog()
        }

        sbEva?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                // Display the current progress_loading of SeekBar
                tvEva?.text = "$i"
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                // Do something

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                // Do something

            }
        })

        rgSatO2AmbO2Types?.setOnCheckedChangeListener { _, i ->
            when(i){
                R.id.rbLN -> fillSpinner(spiO2AmbO2Value, R.array.satO2AmbO2ValuesLN)
                R.id.rbVMK -> fillSpinner(spiO2AmbO2Value, R.array.satO2AmbO2ValuesVMK)
            }
        }

        initData()
    }

    override fun onResume() {
        super.onResume()
        initData()
        inactivityController!!.isInactive()
    }

    private fun initData(){
        //patient name
        val json = intent.getStringExtra(Tags.PATIENT.toString())
        patient = Gson().fromJson(json, CensusPatient::class.java)

        toolbar.title = patient?.cenNamePatient
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        toolbar.setNavigationOnClickListener {
            onBackPressed()
        }
        //Get measurement fields and vital signs
        requestAndCreateVitalSignsHTTPVolley(NetworkPaths.API_URL_VITALSIGN)

        //room number and nhc
        tvRoom?.text = patient?.cenNumRoom
        tvNHC?.text = patient?.cenNumHistory
        //Set date and hour
        etDate?.setText(TimeCustomUtils.currentDate)
        etHour?.setText(TimeCustomUtils.currentHour)

        fillSpinner(spiO2AmbO2Value, R.array.satO2AmbO2ValuesLN)
        fillSpinner(spiDeposic, R.array.deposicValues)

    }
    private fun fillSpinner(spin:Spinner?, textArrayResId:Int){
        val adapter = ArrayAdapter.createFromResource(
            this,
            textArrayResId, R.layout.support_simple_spinner_dropdown_item
        )
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        spin?.adapter = adapter
    }


    private fun hideSoftKeyboard(view: View) {
        val inputMethodManager = this.getSystemService(
            Activity.INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(
            this.currentFocus!!.windowToken, 0
        )

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_constant_register_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.iReport->{
                val intent = Intent(this, VitalSignsMeasurementReportActivity::class.java)
                intent.putExtra(Tags.PATIENT_ID.toString(), patient!!.cenId.toString())
                startActivity(intent)

            }
        }
        return true
    }

    private fun showRegisterDialog(){
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.measure_dialog, null)
        val dialogBuilder = AlertDialog.Builder(this)


        dialogBuilder
            .setCancelable(false)
            .setView(mDialogView)

        val alert = dialogBuilder.create()
        alert.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        alert.show()

        alert.btnRegisterDialog.setOnClickListener {
            try{
                alert.dismiss()
                measurementFields?.containsMeasurementsToPost()
                //postMeasurementHTTPVolley()
                constantRegisterController!!.register(patient!!, msrDateHourStart, measurementFields!!)
            }catch (e: NotValidDataException){
                Toast.makeText(this, R.string.incorrect_values, Toast.LENGTH_LONG).show()
            }catch (e: NotMeasuresException){
                finish()
                Toast.makeText(this, getString(R.string.mesures_no_enregistrades), Toast.LENGTH_LONG).show()
            }
        }
        alert.btnCancelDialog.setOnClickListener{
            alert.dismiss()
        }



    }

    override fun onPause() {
        super.onPause()
        inactivityController!!.detectInactivityAndExecuteTimer()
    }




    private fun requestAndCreateVitalSignsHTTPVolley(url: String) {
        val gson = Gson()
        val vitalSigns = customSharedPreferences?.getValueString(Tags.VITAL_SIGNS_LIST.toString())
        if (!vitalSigns.isNullOrEmpty()){
            vitalSignsList = gson.fromJson(vitalSigns, VitalSignsList::class.java)
            measurementFields = MeasurementsFactory.createMeasurementsFields(this, vitalSignsList!!)
            return
        }
        val customProgressDialog = CustomProgressDialog(applicationContext, this, R.layout.progress_loading)
        customProgressDialog.setDialog(true)
        val jsonObjRequest = object : StringRequest(

            Method.GET, url, Response.Listener { response ->
                val res = "{ \"vitalSigns\" : $response}"
                customSharedPreferences!!.save(Tags.VITAL_SIGNS_LIST.toString(), res)
                vitalSignsList = gson.fromJson(res, VitalSignsList::class.java)
                measurementFields = MeasurementsFactory.createMeasurementsFields(this, vitalSignsList!!)
                customProgressDialog.setDialog(false)

            },
            Response.ErrorListener { error -> VolleyLog.d("GET ERROR", "Error: " + error.message) }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/json"
                val auth = "Bearer " + customSharedPreferences!!.getValueString(Tags.ACCES_TOKEN.toString())
                headers["Authorization"] = auth
                return headers
            }

            override fun deliverError(error: VolleyError?) {
                customProgressDialog.setDialog(false)
                Toast.makeText(applicationContext, VolleyErrorHelper.getMessage(error!!, applicationContext, this@ConstantRegisterActivity), Toast.LENGTH_LONG).show()
            }

        }

        VolleySingleton.getInstance(this).addToRequestQueue(jsonObjRequest)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }




}

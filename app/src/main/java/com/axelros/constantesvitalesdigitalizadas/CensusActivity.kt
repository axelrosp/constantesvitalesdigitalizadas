package com.axelros.constantesvitalesdigitalizadas

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.axelros.constantesvitalesdigitalizadas.controllers.patients.PatientsController
import com.axelros.constantesvitalesdigitalizadas.factory.controllersFactory.ControllersFactory
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import kotlinx.android.synthetic.main.activity_patients.*
import android.content.Intent
import com.axelros.constantesvitalesdigitalizadas.controllers.InactivityController.InactivityController


class CensusActivity : AppCompatActivity() {

    private var layoutManager: RecyclerView.LayoutManager? = null
    private var customSharedPreferences: CustomSharedPreferences? = null
    private var patientsController: PatientsController? = null

    private var inactivityController:InactivityController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_patients)
        init()
    }

    private fun init() {
        patientsController = ControllersFactory.createPatientsController(this, applicationContext)
        customSharedPreferences = CustomSharedPreferences(this)
        inactivityController = InactivityController(customSharedPreferences!!, this, this)

        toolbar.title = getString(R.string.PatientsTitle)
        setSupportActionBar(toolbar)

        setupPatientsList()

        initData()
    }

    private fun setupPatientsList(){
        rvPatientsList?.setHasFixedSize(true)

        layoutManager = GridLayoutManager(this, 2)
        rvPatientsList?.layoutManager = layoutManager

        refreshCensusConf()
    }

    private fun initData() {
        patientsController!!.drawPatients()
    }

    override fun onResume() {
        super.onResume()
        if(!inactivityController!!.isInactive()) {
            patientsController!!.drawPatients()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        logout()
        finish()
    }

    private fun refreshCensusConf() {
        val mHandler = Handler()
        swipeToRefresh?.setOnRefreshListener {
            val mRunnable = Runnable {
                patientsController!!.drawPatients()
                swipeToRefresh?.isRefreshing = false
            }
            mHandler.post(
                mRunnable
            )
        }
    }

    //Toolbar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_patients_activity, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.iLogout ->{
                logout()
                finish()
            }
        }
        return true
    }

    private fun logout(){
        Thread {
            val i = Intent(applicationContext, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(i)
            customSharedPreferences!!.clearSharedPreference()
            finish()
        }.start()


    }

    override fun onPause() {
        super.onPause()
        inactivityController!!.detectInactivityAndExecuteTimer()
    }


}

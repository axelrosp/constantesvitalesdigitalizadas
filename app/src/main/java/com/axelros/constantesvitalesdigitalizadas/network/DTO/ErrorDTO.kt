package com.axelros.constantesvitalesdigitalizadas.network.DTO

import org.json.JSONObject

data class ErrorDTO(
    val error: String,
    val error_description: String) {

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()
        jsonObject.put("error", String)
        jsonObject.put("error_description", String)
        return jsonObject
    }
}
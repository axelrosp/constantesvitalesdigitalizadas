package com.axelros.constantesvitalesdigitalizadas.network.volleyController.vitalSignMeasurement

import android.app.Activity
import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.VitalSignsMeasurementReportActivity
import com.axelros.constantesvitalesdigitalizadas.model.DTO.MeasurementFieldDTO
import com.axelros.constantesvitalesdigitalizadas.model.Measurement
import com.axelros.constantesvitalesdigitalizadas.model.MeasurementFields
import com.axelros.constantesvitalesdigitalizadas.model.VitalSignMeasurement

interface iVitalSignMeasurementController {
    fun postVitalSignMeasurement(vitalSignMeasurement: VitalSignMeasurement, context: Context, view: Activity)
    fun postIfVitalSignsMeasurements(measurementFields: MeasurementFields, measurement: Measurement, context: Context, view:Activity)
    fun getMeasurementReportOfPatient(view: VitalSignsMeasurementReportActivity, context:Context, cenId: String)
}
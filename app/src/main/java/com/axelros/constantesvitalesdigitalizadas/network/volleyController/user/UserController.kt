package com.axelros.constantesvitalesdigitalizadas.network.volleyController.user

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper.VolleyErrorHelper
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import java.util.HashMap

class UserController{

    companion object: iUserController {
        override fun getUserByUsername(username: String, context:Context, view: Activity) {
            val customSharedPreferences = CustomSharedPreferences(context)
            val jsonObjRequest = object : StringRequest(

                Request.Method.GET,
                NetworkPaths.API_URL_USERS_BY_USERNAME + username, Response.Listener { response ->
                    customSharedPreferences?.save(Tags.USER.toString(), response)


                },
                Response.ErrorListener { error -> VolleyLog.d("GET ERROR", "Error: " + error.message) }) {

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    val auth = "Bearer " + customSharedPreferences!!.getValueString(Tags.ACCES_TOKEN.toString())
                    headers["Authorization"] = auth
                    return headers
                }

                override fun deliverError(error: VolleyError?) {
                    Toast.makeText(context, VolleyErrorHelper.getMessage(error!!, context, view), Toast.LENGTH_LONG).show()
                }

            }

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjRequest)
        }


    }




}
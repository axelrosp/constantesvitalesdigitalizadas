package com.axelros.constantesvitalesdigitalizadas.network

import android.content.Context
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity

class NetworkPaths{

    companion object {
        //IP_MOBIL
        //val API_IP = "http://192.168.43.235:8081"

        //CASA
        val API_IP = "http://192.168.1.48:8081"

        val API_URL = "${API_IP}/api"

        //authentication
        val API_URL_LOGIN= "$API_IP/oauth/token"

        //Census
        val API_URL_CENSUS = "$API_URL/census/"
        val API_URL_CENSUS_BY_FLOOR = "${API_URL_CENSUS}byFloor/"

        //Measurements
        val API_URL_MEASUREMENT = "$API_URL/measurements/"

        //Users
        val API_URL_USERS = "$API_URL/users/"
        val API_URL_USERS_BY_USERNAME = API_URL_USERS + "byUsername/"

        //Vital signs
        val API_URL_VITALSIGN = "$API_URL/vitalsigns/"

        //Vital signs measurements
        val API_URL_VITALSIGNMEASUREMENT = "$API_URL/vitalsignmeasurements/"
        val API_URL_VITALSIGNMEASUREMENT_BY_CENID = API_URL_VITALSIGNMEASUREMENT +"byCenId/"

    }
}

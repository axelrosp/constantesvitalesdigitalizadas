package com.axelros.constantesvitalesdigitalizadas.network.volleyController.user

import android.app.Activity
import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.LoginActivity

interface iUserController {
    fun getUserByUsername(username: String, context: Context, view:Activity)
}
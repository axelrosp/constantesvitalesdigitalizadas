package com.axelros.constantesvitalesdigitalizadas.network.volleyController.vitalSignMeasurement

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.VitalSignsMeasurementReportActivity
import com.axelros.constantesvitalesdigitalizadas.adapters.CustomAdapterMeasurementReport
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.interfaces.ClickListener
import com.axelros.constantesvitalesdigitalizadas.model.*
import com.axelros.constantesvitalesdigitalizadas.model.DTO.MeasurementFieldDTO
import com.axelros.constantesvitalesdigitalizadas.model.DTO.VitalSignMeasurementDTO
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper.VolleyErrorHelper
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.utils.CustomProgressDialog
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_vital_signs_measurement_report.*
import java.util.HashMap

class VitalSignMeasurementController {
    companion object:iVitalSignMeasurementController {
        override fun postVitalSignMeasurement(vitalSignMeasurement: VitalSignMeasurement, context:Context, view: Activity){
            val customSharedPreferences = CustomSharedPreferences(context)

            val js = vitalSignMeasurement.toJson()

            val jsonObjReq = object : JsonObjectRequest(
                Method.POST, NetworkPaths.API_URL_VITALSIGNMEASUREMENT, js,
                Response.Listener { response -> Log.d("POST DONE", "$response") },
                Response.ErrorListener { error -> VolleyLog.d("POST ERROR", "Error: " + error.message) }) {

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    val auth = "Bearer " + customSharedPreferences!!.getValueString(Tags.ACCES_TOKEN.toString())
                    headers["Authorization"] = auth
                    return headers
                }
                override fun deliverError(error: VolleyError?) {
                    Toast.makeText(context, VolleyErrorHelper.getMessage(error!!, context, view), Toast.LENGTH_LONG).show()
                }
            }
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq)

        }

        override fun postIfVitalSignsMeasurements(measurementFields: MeasurementFields, measurement: Measurement, context:Context, view:Activity) {
            val iterator = measurementFields?.getIterator()
            iterator?.forEach { m ->
                if((m.etMeasurement != null && m.etMeasurement.text?.isNotEmpty()!!) ||
                    ((m.spMeasurement != null && m.spMeasurement.selectedItem.toString() != "--") &&
                            (m.etMeasurement != null && m.etMeasurement.text?.isNotEmpty()!!)) ||
                    (m.tvMeasurement != null && m.tvMeasurement.text?.isNotEmpty()!!) ||
                    (m.spMeasurement != null && m.spMeasurement.selectedItem.toString() != "--") &&
                            (m.etMeasurement == null)
                ){
                     createVitalSignMeasurement(m, measurement, context, view)
                }
            }

        }

        override fun getMeasurementReportOfPatient(view: VitalSignsMeasurementReportActivity, context:Context, cenId: String) {
            val customProgressDialog = CustomProgressDialog(context, view, R.layout.progress_loading)
            val customSharedPreferences = CustomSharedPreferences(context)

            val url = NetworkPaths.API_URL_VITALSIGNMEASUREMENT_BY_CENID + cenId

            customProgressDialog.setDialog(true)
            val jsonObjRequest = object : StringRequest(
                Request.Method.GET,
                url, Response.Listener { response ->
                    val gson = Gson()
                    val res = "{ \"vitalSignMeasurementList\" : $response}"
                    val vitalSignMeasurementsList = gson.fromJson(res, VitalSignMeasurementsList::class.java)

                    val measurementMap: LinkedHashMap<Measurement, MutableCollection<VitalSignMeasurementDTO>> = LinkedHashMap()

                    vitalSignMeasurementsList.vitalSignMeasurementList?.forEach {vsm ->
                        if(!measurementMap.containsKey(vsm.measurement)){
                            measurementMap.put(
                                vsm.measurement, arrayListOf()
                            )
                        }
                        measurementMap[vsm.measurement]?.add(
                            VitalSignMeasurementDTO(
                                vsm.measurement, vsm.vitalSign, vsm.vsmFilter, vsm.vsmIsManual, vsm.vsmSystem, vsm.vsmValue, vsm.vsmNemotechnical)
                        )

                    }


                    if (measurementMap.values.isEmpty()) {
                        view.lottieEmpty.visibility = View.VISIBLE
                        view.rvMeasurementsList.visibility = View.GONE
                        view.lottieEmpty.playAnimation()
                    } else {
                        view.lottieEmpty.visibility = View.GONE
                        view.rvMeasurementsList.visibility = View.VISIBLE

                        val adapter = CustomAdapterMeasurementReport(measurementMap.values, object : ClickListener {
                            override fun onClick(view: View, index: Int) {


                            }
                        })

                        view.rvMeasurementsList?.adapter = adapter
                    }
                    customProgressDialog.setDialog(false)
                },
                Response.ErrorListener { error ->
                    VolleyLog.d("GET ERROR", "Error: " + error.message)}) {

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    val auth = "Bearer " + customSharedPreferences!!.getValueString(Tags.ACCES_TOKEN.toString())
                    headers["Authorization"] = auth
                    return headers
                }
                override fun deliverError(error: VolleyError?) {
                    Toast.makeText(context, VolleyErrorHelper.getMessage(error!!, context, view), Toast.LENGTH_LONG).show()
                    customProgressDialog.setDialog(false)
                }

            }

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjRequest)
        }

        private fun createVitalSignMeasurement(msrField: MeasurementFieldDTO, measurement: Measurement, context:Context, view:Activity) {
            val vitalSignMeasurement: VitalSignMeasurement?
            val vitalSign: VitalSign? = msrField.vitalSign
            var vsmIsManual = false
            var vsmFilter = 0.0
            val vsmNemotechnical:String = msrField.vsNemotechnical
            var vsmSystem = ""
            var vsmValue = ""

            //Get checkbox value
            if (msrField.cbMeasurement != null) {
                vsmIsManual = msrField.cbMeasurement.isChecked
            }
            //Get Filter if it have
            if(msrField.etMeasurement != null && msrField.spMeasurement != null &&
                msrField.spMeasurement.selectedItem != null && msrField.spMeasurement.selectedItem.toString() != "--"){
                vsmFilter = msrField.spMeasurement.selectedItem.toString().toDouble()
            }
            //Get radio button value
            if(msrField.rgMeasruement != null){
                var id = msrField.rgMeasruement.checkedRadioButtonId
                var rb = msrField.rgMeasruement.findViewById(id) as RadioButton
                vsmSystem = rb.text.toString()
            }

            //Get Value
            if(msrField.etMeasurement != null){
                vsmValue = msrField.etMeasurement.text.toString()
            }
            else if(msrField.spMeasurement != null){
                vsmValue = msrField.spMeasurement.selectedItem.toString()
            }
            else if(msrField.tvMeasurement != null && msrField.tvMeasurement.text?.isNotEmpty()!!){
                vsmValue = msrField.tvMeasurement.text.toString()

            }

            vitalSignMeasurement = VitalSignMeasurement(
                0,
                measurement!!,
                vitalSign!!,
                vsmFilter,
                vsmIsManual,
                vsmNemotechnical,
                vsmSystem,
                vsmValue
            )
            postVitalSignMeasurement(vitalSignMeasurement, context, view)

        }
    }
}
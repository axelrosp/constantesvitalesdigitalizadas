package com.axelros.constantesvitalesdigitalizadas.network.volleyController.census

import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.CensusActivity

interface iCensusController {
    fun fillCensusPatientsListByFloor(view: CensusActivity, context: Context)
}
package com.axelros.constantesvitalesdigitalizadas.network.volleyController.measurement

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.axelros.constantesvitalesdigitalizadas.ConstantRegisterActivity
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.model.*
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.network.volleyController.vitalSignMeasurement.VitalSignMeasurementController
import com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper.VolleyErrorHelper
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.utils.CustomProgressDialog
import com.axelros.constantesvitalesdigitalizadas.utils.TimeCustomUtils
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_constant_register.*
import java.util.HashMap

class MeasurementController {
    companion object:iMeasurementController {
        override fun postMeasurementHTTPVolley(view:ConstantRegisterActivity, context: Context, patient: CensusPatient, msrDateHourStart:String, measurementFields: MeasurementFields){
            val customProgressDialog = CustomProgressDialog(context, view, R.layout.progress_register_measurement)
            val customSharedPreferences = CustomSharedPreferences(context)
            val gson = Gson()
            val userRegistered = gson.fromJson(customSharedPreferences?.getValueString(Tags.USER.toString()), UserRegistered::class.java)


            //Test that data is correct posted
            var dateByProfessional = TimeCustomUtils.convertDateHourToTimeStamp(view.etDate.text.toString(), view.etHour.text.toString())
            val measurement = Measurement(
                0,
                patient,
                TimeCustomUtils.getCurrentTimeStamp,
                msrDateHourStart,
                dateByProfessional,
                userRegistered,
                "",
                ""
            )

            val js = measurement.toJson()

            customProgressDialog.setDialog(true)
            // Make request for JSONObject
            val jsonObjReq = object : JsonObjectRequest(
                Request.Method.POST, NetworkPaths.API_URL_MEASUREMENT, js,
                Response.Listener { response ->
                    measurement?.msrId = response.get("msrId").toString().toInt()
                    VitalSignMeasurementController.postIfVitalSignsMeasurements(measurementFields, measurement, context, view)
                    customProgressDialog.setDialog(false)
                    Toast.makeText(context, context.resources.getString(R.string.mesures_enregistrades), Toast.LENGTH_LONG).show()
                    view.finish()
                },
                Response.ErrorListener { error ->
                    VolleyLog.d("POST ERROR", "Error: " + error.message)
                    customProgressDialog.setDialog(false)
                }) {

                /**
                 * Passing some request headers
                 */
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    val auth = "Bearer " + customSharedPreferences!!.getValueString(Tags.ACCES_TOKEN.toString())
                    headers["Authorization"] = auth
                    return headers
                }

                override fun deliverError(error: VolleyError?) {
                    customProgressDialog.setDialog(false)
                    Toast.makeText(context, VolleyErrorHelper.getMessage(error!!, context, view), Toast.LENGTH_LONG).show()
                }
            }
            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjReq)
        }




    }
}
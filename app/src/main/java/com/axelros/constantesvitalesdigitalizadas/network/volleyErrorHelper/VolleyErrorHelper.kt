package com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper

import android.app.Activity
import android.content.Context
import com.android.volley.*
import com.google.gson.Gson
import com.android.volley.VolleyError
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.network.DTO.ErrorDTO
import com.axelros.constantesvitalesdigitalizadas.controllers.security.token.RefreshTokenController
class VolleyErrorHelper {
    companion object {
        fun getMessage(error: VolleyError, context: Context, view: Activity): String {
            return when {
                error is TimeoutError -> context.getResources().getString(R.string.generic_server_down)
                error is ClientError -> clientError(error, context)
                error is AuthFailureError -> authFailureError(error, context, view)
                //isNetworkProblem(error) -> context.getResources().getString(R.string.no_internet)
                else -> context.getResources().getString(R.string.generic_error)
            }
        }

        private fun clientError(error: VolleyError, context:Context) : String{
            val code = error.networkResponse.statusCode
            val body = String(error.networkResponse.data)


            return when{
                code == 400 -> {
                    val gson = Gson()
                    val errorDTO = gson.fromJson(body, ErrorDTO::class.java)
                    if(errorDTO.error == "invalid_grant"){
                        return context.resources.getString(R.string.invalid_username_password)
                    }
                    ""
                }
                else -> context.resources.getString(R.string.generic_error)
            }
        }

        private fun authFailureError(error: VolleyError, context:Context, view: Activity) : String{
            val code = error.networkResponse.statusCode
            val body = String(error.networkResponse.data)


            return when{
                code == 401 -> {
                    RefreshTokenController.refreshAccesToken(context, view)
                    ""
                }
                else -> context.resources.getString(R.string.generic_error)
            }
        }

    }

}
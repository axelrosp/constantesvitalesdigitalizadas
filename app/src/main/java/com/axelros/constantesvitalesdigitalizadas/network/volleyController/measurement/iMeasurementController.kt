package com.axelros.constantesvitalesdigitalizadas.network.volleyController.measurement

import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.ConstantRegisterActivity
import com.axelros.constantesvitalesdigitalizadas.model.CensusPatient
import com.axelros.constantesvitalesdigitalizadas.model.MeasurementFields

interface iMeasurementController {
    fun postMeasurementHTTPVolley(view: ConstantRegisterActivity, context: Context, patient: CensusPatient, msrDateHourStart:String, measurementFields: MeasurementFields)

}
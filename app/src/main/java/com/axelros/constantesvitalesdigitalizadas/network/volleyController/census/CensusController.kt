package com.axelros.constantesvitalesdigitalizadas.network.volleyController.census

import android.content.Context
import android.content.Intent
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.axelros.constantesvitalesdigitalizadas.ConstantRegisterActivity
import com.axelros.constantesvitalesdigitalizadas.CensusActivity
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.adapters.CustomAdapterPatient
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.interfaces.ClickListener
import com.axelros.constantesvitalesdigitalizadas.model.Census
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper.VolleyErrorHelper
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.utils.CustomProgressDialog
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_patients.*
import java.util.*

class CensusController {
    companion object: iCensusController {
        override fun fillCensusPatientsListByFloor(view:CensusActivity, context: Context) {
            val customProgressDialog = CustomProgressDialog(context, view, R.layout.progress_loading)
            val customSharedPreferences = CustomSharedPreferences(context)
            val url = NetworkPaths.API_URL_CENSUS_BY_FLOOR + customSharedPreferences!!.getValueInt(Tags.FLOOR.toString())

            customProgressDialog.setDialog(true)
            val jsonObjRequest = object : StringRequest(
                Request.Method.GET,
                url, Response.Listener { response ->
                    val gson = Gson()
                    val res = "{ \"censuses\" : $response}"
                    val census = gson.fromJson(res, Census::class.java)

                    if (census.censuses!!.isEmpty()) {
                        view.lottieEmpty.visibility = View.VISIBLE
                        view.rvPatientsList.visibility = View.GONE

                        view.lottieEmpty.playAnimation()
                    } else {
                        view.lottieEmpty.visibility = View.GONE
                        view.rvPatientsList.visibility = View.VISIBLE

                        val adapter = CustomAdapterPatient(census?.censuses!!, object : ClickListener {
                            var mLastClickTime:Long = 0;
                            override fun onClick(onClickView: View, index: Int) {
                                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                                    return
                                }
                                mLastClickTime = SystemClock.elapsedRealtime()
                                val patient = census?.censuses?.get(index)?.census
                                val intent = Intent(context, ConstantRegisterActivity::class.java)
                                val json = Gson().toJson(patient)
                                intent.putExtra(Tags.PATIENT.toString(), json)
                                context.startActivity(intent)
                            }
                        })

                        view.rvPatientsList?.adapter = adapter
                    }

                    customProgressDialog.setDialog(false)

                },
                Response.ErrorListener { error ->
                    VolleyLog.d("GET ERROR", "Error: " + error.message)
                    customProgressDialog.setDialog(false)
                }) {

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/json"
                    val auth = "Bearer " + customSharedPreferences!!.getValueString(Tags.ACCES_TOKEN.toString())
                    headers["Authorization"] = auth
                    return headers
                }

                override fun deliverError(error: VolleyError?) {
                    customProgressDialog.setDialog(false)
                    Toast.makeText(context, VolleyErrorHelper.getMessage(error!!, context, view), Toast.LENGTH_LONG).show()
                }
            }

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjRequest)
        }

    }
}
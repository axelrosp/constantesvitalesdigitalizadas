package com.axelros.constantesvitalesdigitalizadas.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager

class CustomProgressDialog(val context: Context, val view:AppCompatActivity, layout: Int) {
    val builder = AlertDialog.Builder(view)
    val dialog = builder.setView(layout).create()!!

    fun setDialog(show: Boolean) {
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        if (show) {
            dialog.show()
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
        }
        else{
            view.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            dialog.dismiss()
        }
    }
}
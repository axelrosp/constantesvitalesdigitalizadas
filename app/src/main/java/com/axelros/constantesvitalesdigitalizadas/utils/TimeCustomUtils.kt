package com.axelros.constantesvitalesdigitalizadas.utils

import android.annotation.SuppressLint
import android.text.format.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class TimeCustomUtils {
    companion object {
        //Set end session time for security
        val TIME_END_SESSION_MILISECONDS:Long = 10000

        val currentDate: String
            @SuppressLint("SimpleDateFormat")
            get() {
                val date = Calendar.getInstance().time
                val dateFormat = SimpleDateFormat("yyyy-MM-dd")
                return dateFormat.format(date)
            }


        val currentHour: String
            @SuppressLint("SimpleDateFormat")
            get() {
                val date = Calendar.getInstance().time
                val dateFormat = SimpleDateFormat("HH:mm")
                return dateFormat.format(date)
            }

        val getCurrentTimeStamp: String
            @SuppressLint("SimpleDateFormat")
            get() {
                val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS")
                val date = simpleDateFormat.format(Date())
                return date

            }
        fun convertDateHourToTimeStamp(date:String, hour:String):String{
            return "${date}T$hour"
        }

        @SuppressLint("SimpleDateFormat")
        fun convertTimeStampToHour(date:String):String{
            try {
                val tz = TimeZone.getTimeZone("UTC")
                val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                df.timeZone = tz
                val dt = df.parse(date)

                val dfO = SimpleDateFormat("HH:mm")
                dfO.timeZone = tz
                return dfO.format(dt)
            } catch (ex: ParseException) {
                println(ex)
            }
            return ""
        }


        @SuppressLint("SimpleDateFormat")
        fun getDateAndHourFromTimeStamp(date:String):String{
            try {
                val tz = TimeZone.getTimeZone("UTC")
                val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm")
                df.timeZone = tz
                val dt = df.parse(date)
                return df.format(dt).replace("T", " ")
            } catch (ex: ParseException) {
                println(ex)
            }
            return ""
        }

    }
}
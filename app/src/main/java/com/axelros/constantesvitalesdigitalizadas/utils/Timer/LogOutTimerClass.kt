package com.axelros.constantesvitalesdigitalizadas.utils.Timer


import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import java.util.*

class LogOutTimerTask(val context: Context) : TimerTask() {
    override fun run() {
        val customSharedPreferences = CustomSharedPreferences(context)
        customSharedPreferences.save(Tags.END_SESSION.toString(), 1)
    }
}
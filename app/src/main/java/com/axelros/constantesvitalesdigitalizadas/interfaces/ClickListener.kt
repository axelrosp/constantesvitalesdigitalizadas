package com.axelros.constantesvitalesdigitalizadas.interfaces

import android.view.View

interface ClickListener {
    fun onClick(view: View, index:Int)
}
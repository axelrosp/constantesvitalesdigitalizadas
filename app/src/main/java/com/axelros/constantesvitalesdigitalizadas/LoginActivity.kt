package com.axelros.constantesvitalesdigitalizadas


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.widget.ArrayAdapter
import com.axelros.constantesvitalesdigitalizadas.controllers.security.login.LoginController
import com.axelros.constantesvitalesdigitalizadas.factory.controllersFactory.ControllersFactory
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {

    private var customSharedPreferences: CustomSharedPreferences? = null
    private var loginController:LoginController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    private fun init() {
        loginController = ControllersFactory.createLoginController(this, applicationContext)
        customSharedPreferences = CustomSharedPreferences(this)
        customSharedPreferences!!.clearSharedPreference()

        toolbar?.setTitle(getString(R.string.AccesTitle))
        setSupportActionBar(toolbar)
        // Create an ArrayAdapter using the string array and a default spinner layout
        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.floors_array, R.layout.support_simple_spinner_dropdown_item
        )
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
        // Apply the adapter to the spinner
        spiFloor?.adapter = adapter

        btnLogin?.setOnClickListener {
            loginController!!.login()

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_login_activity, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
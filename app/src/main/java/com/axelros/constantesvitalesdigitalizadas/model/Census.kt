package com.axelros.constantesvitalesdigitalizadas.model

import com.axelros.constantesvitalesdigitalizadas.model.DTO.CensusDTO

class Census(censuses:ArrayList<CensusDTO>){
    var censuses: ArrayList<CensusDTO>? = null

    init {
        this.censuses = censuses
    }

}

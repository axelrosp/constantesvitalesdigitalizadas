package com.axelros.constantesvitalesdigitalizadas.model

import org.json.JSONObject

data class Measurement(
    var msrId: Int,
    val census: CensusPatient,
    val msrDateHourEnd: String,
    val msrDateHourStart: String,
    val msrDateHourSetByProfessional:String,
    val userRegistered: UserRegistered,
    val msrState: String,
    val msrObservations: String)
{

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()

        jsonObject.put("msrId", msrId)
        jsonObject.put("census", census.toJson())
        jsonObject.put("msrDateHourEnd", msrDateHourEnd)
        jsonObject.put("msrDateHourStart", msrDateHourStart)
        jsonObject.put("msrDateHourSetByProfessional", msrDateHourSetByProfessional)
        jsonObject.put("userRegistered", userRegistered.toJson())
        jsonObject.put("msrState", msrState)
        jsonObject.put("msrObservations", msrObservations)

        return jsonObject
    }
}
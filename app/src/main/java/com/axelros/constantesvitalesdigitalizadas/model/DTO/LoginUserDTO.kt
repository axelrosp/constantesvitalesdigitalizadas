package com.axelros.constantesvitalesdigitalizadas.model.DTO

data class LoginUserDTO(
    val access_token: String,
    val apellido: String,
    val email: String,
    val expires_in: Int,
    val info_adicional: String,
    val jti: String,
    val nombre: String,
    val refresh_token: String,
    val scope: String,
    val token_type: String,
    val username: String
)
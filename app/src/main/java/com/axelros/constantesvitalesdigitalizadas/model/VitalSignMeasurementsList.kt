package com.axelros.constantesvitalesdigitalizadas.model

class VitalSignMeasurementsList(vitalSignMeasurementList:ArrayList<VitalSignMeasurement>){
    var vitalSignMeasurementList: ArrayList<VitalSignMeasurement>? = null

    init {
        this.vitalSignMeasurementList = vitalSignMeasurementList
    }

}
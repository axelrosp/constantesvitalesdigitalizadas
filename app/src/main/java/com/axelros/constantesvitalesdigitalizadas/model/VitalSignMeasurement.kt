package com.axelros.constantesvitalesdigitalizadas.model

import org.json.JSONObject

data class VitalSignMeasurement(
    val vsmId: Int,
    var measurement: Measurement,
    val vitalSign: VitalSign,
    val vsmFilter: Double,
    val vsmIsManual: Boolean,
    val vsmNemotechnical: String,
    val vsmSystem: String,
    val vsmValue: String){

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()

        jsonObject.put("vsmId", vsmId)
        jsonObject.put("measurement", measurement.toJson())
        jsonObject.put("vitalSign", vitalSign.toJson())
        jsonObject.put("vsmFilter", vsmFilter)
        jsonObject.put("vsmIsManual", vsmIsManual)
        jsonObject.put("vsmNemotechnical", vsmNemotechnical)
        jsonObject.put("vsmSystem", vsmSystem)
        jsonObject.put("vsmValue", vsmValue)

        return jsonObject
    }
}
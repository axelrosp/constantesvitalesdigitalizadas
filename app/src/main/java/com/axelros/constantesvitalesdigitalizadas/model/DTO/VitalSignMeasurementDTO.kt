package com.axelros.constantesvitalesdigitalizadas.model.DTO

import com.axelros.constantesvitalesdigitalizadas.model.Measurement
import com.axelros.constantesvitalesdigitalizadas.model.VitalSign

class VitalSignMeasurementDTO (
    val measurement: Measurement,
    val vitalSign: VitalSign,
    val vsmFilter: Double,
    val vsmIsManual: Boolean,
    val vsmSystem: String,
    val vsmValue: String,
    val vsmNemotechnical: String)
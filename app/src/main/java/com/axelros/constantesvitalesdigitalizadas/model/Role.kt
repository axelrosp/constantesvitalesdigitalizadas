package com.axelros.constantesvitalesdigitalizadas.model

import org.json.JSONObject

data class Role(
    val id: Int,
    val nombre: String)
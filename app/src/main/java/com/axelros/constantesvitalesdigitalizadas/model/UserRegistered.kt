package com.axelros.constantesvitalesdigitalizadas.model

import android.util.Log
import org.json.JSONObject

data class UserRegistered(
    val id: Int,
    val apellido: String,
    val email: String?,
    val enabled: Boolean,
    val nombre: String,
    val password: String,
    val username: String){

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()

        jsonObject.put("id", id)
        jsonObject.put("apellido", apellido)
        jsonObject.put("email", email)
        jsonObject.put("enabled", enabled)
        jsonObject.put("nombre", nombre)
        jsonObject.put("password", password)
        jsonObject.put("username", username)

        Log.d("USER REGISTERED", jsonObject.toString())
        return jsonObject
    }
}
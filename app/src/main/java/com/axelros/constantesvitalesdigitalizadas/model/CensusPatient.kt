package com.axelros.constantesvitalesdigitalizadas.model

import org.json.JSONObject

data class CensusPatient(
    val cenId: Int,
    val cenNamePatient: String,
    val cenNumFloor: Int,
    val cenNumHistory: String,
    val cenNumRoom: String,
    val cenService: String,
    val cenSpeciality: String){

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()

        jsonObject.put("cenId", cenId)
        jsonObject.put("cenNamePatient", cenNamePatient)
        jsonObject.put("cenNumFloor", cenNumFloor)
        jsonObject.put("cenNumHistory", cenNumHistory)
        jsonObject.put("cenNumRoom", cenNumRoom)
        jsonObject.put("cenService", cenService)
        jsonObject.put("cenSpeciality", cenSpeciality)

        return jsonObject
    }
}
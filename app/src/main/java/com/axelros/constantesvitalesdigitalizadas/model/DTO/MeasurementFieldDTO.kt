package com.axelros.constantesvitalesdigitalizadas.model.DTO

import android.widget.*
import com.axelros.constantesvitalesdigitalizadas.model.VitalSign

data class MeasurementFieldDTO(
    val vsName: String,
    val vsNemotechnical: String,
    val etMeasurement: EditText?,
    val cbMeasurement: CheckBox?,
    val rgMeasruement: RadioGroup?,
    val spMeasurement: Spinner?,
    val tvMeasurement: TextView?,
    val vitalSign: VitalSign?
    )
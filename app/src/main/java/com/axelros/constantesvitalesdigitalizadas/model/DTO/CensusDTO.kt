package com.axelros.constantesvitalesdigitalizadas.model.DTO

import com.axelros.constantesvitalesdigitalizadas.model.CensusPatient

data class CensusDTO(
    val census: CensusPatient,
    val msrDateHourEnd: String
)
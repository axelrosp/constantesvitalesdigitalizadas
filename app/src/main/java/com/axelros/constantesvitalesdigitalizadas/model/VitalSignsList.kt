package com.axelros.constantesvitalesdigitalizadas.model

class VitalSignsList (vitalSigns:ArrayList<VitalSign>){
    var vitalSigns: ArrayList<VitalSign>? = null

    init {
        this.vitalSigns = vitalSigns
    }

}
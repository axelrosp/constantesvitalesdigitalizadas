package com.axelros.constantesvitalesdigitalizadas.model

import org.json.JSONObject

data class VitalSign(
    val vsId: Int,
    val vitalSignType: VitalSignType,
    val vsName: String,
    val vsNemotechnical: String,
    val vsLimitMaxValue: Int?,
    val vsLimitMinValue: Int?){

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()

        jsonObject.put("vsId", vsId)
        jsonObject.put("vitalSignType", vitalSignType.toJson())
        jsonObject.put("vsName", vsName)
        jsonObject.put("vsNemotechnical", vsNemotechnical)
        jsonObject.put("vsLimitMaxValue", vsLimitMaxValue)
        jsonObject.put("vsLimitMinValue", vsLimitMinValue)

        return jsonObject
    }
}
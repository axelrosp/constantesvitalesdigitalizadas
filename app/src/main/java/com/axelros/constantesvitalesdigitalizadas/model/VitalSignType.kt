package com.axelros.constantesvitalesdigitalizadas.model

import org.json.JSONObject

data class VitalSignType(
    val vstId: Int,
    val vstName: String){

    fun toJson(): JSONObject {
        val jsonObject = JSONObject()

        jsonObject.put("vstId", vstId)
        jsonObject.put("vstName", vstName)
        return jsonObject
    }
}
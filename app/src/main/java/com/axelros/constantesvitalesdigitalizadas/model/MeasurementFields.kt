package com.axelros.constantesvitalesdigitalizadas.model

import android.util.Log
import com.axelros.constantesvitalesdigitalizadas.exceptions.NotMeasuresException
import com.axelros.constantesvitalesdigitalizadas.exceptions.NotValidDataException
import com.axelros.constantesvitalesdigitalizadas.model.DTO.MeasurementFieldDTO
import java.lang.Exception
import java.util.regex.Pattern

class MeasurementFields {
    private var measurementFields:MutableCollection<MeasurementFieldDTO>? = null

    init {
        measurementFields = mutableListOf()
    }

    fun addMeasurementField(measurementFieldDTO: MeasurementFieldDTO){
        measurementFields?.add(measurementFieldDTO)
    }

    fun getIterator(): MutableIterator<MeasurementFieldDTO>? {
        return measurementFields?.iterator()
    }

    @Throws(Exception::class)
    fun containsMeasurementsToPost():Boolean {
        var hasData = false
        measurementFields?.forEach { m ->
            if ((m.etMeasurement != null && m.etMeasurement.text?.isNotEmpty()!!) ||
                ((m.spMeasurement != null && m.spMeasurement.selectedItem.toString() != "--") &&
                        (m.etMeasurement != null && m.etMeasurement.text?.isNotEmpty()!!)) ||
                (m.tvMeasurement != null && m.tvMeasurement.text?.isNotEmpty()!!) ||
                (m.spMeasurement != null && m.spMeasurement.selectedItem.toString() != "--") &&
                        m.etMeasurement == null
            ) {
                validateData(m)
                hasData = true
            }
        }
        if(!hasData) {
            throw NotMeasuresException("No hi han mesures a registrar")
        }
        return hasData

    }

    @Throws(NotValidDataException::class)
    fun validateData(measurementFieldDTO: MeasurementFieldDTO){
        val etMeasurement = measurementFieldDTO.etMeasurement

        if(etMeasurement != null && etMeasurement.text.isNotEmpty()){
            val value = getIntFromString(etMeasurement.text.toString())
            System.out.println(value)
            if(measurementFieldDTO.vitalSign?.vsLimitMaxValue != null &&
                value > measurementFieldDTO.vitalSign.vsLimitMaxValue.toInt()){

                etMeasurement.error = "No es possible inserir un valor de "+
                        measurementFieldDTO.vsName + " superior a "+ measurementFieldDTO.vitalSign.vsLimitMaxValue
                etMeasurement.hasFocus()
                throw NotValidDataException("Valors incorrectes")

            }
            if(measurementFieldDTO.vitalSign?.vsLimitMinValue != null &&
                value < measurementFieldDTO.vitalSign.vsLimitMinValue){

                etMeasurement.error = "No es possible inserir un valor de "+
                        measurementFieldDTO.vsName + " inferior a "+ measurementFieldDTO.vitalSign.vsLimitMinValue
                throw NotValidDataException("Valors incorrectes")

            }
        }

    }

    private fun getIntFromString(str:String): Int{
        val pattern = Pattern.compile("\\d+")
        val matcher = pattern.matcher(str)
        var result = ""

        if(matcher.find()) {
            result = matcher.group(0)
        }
        Log.d("RESULT COLMENASO", result)
        return result.toInt()
    }
}
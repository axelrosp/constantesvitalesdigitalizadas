package com.axelros.constantesvitalesdigitalizadas.factory.controllersFactory

import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.ConstantRegisterActivity
import com.axelros.constantesvitalesdigitalizadas.LoginActivity
import com.axelros.constantesvitalesdigitalizadas.CensusActivity
import com.axelros.constantesvitalesdigitalizadas.VitalSignsMeasurementReportActivity
import com.axelros.constantesvitalesdigitalizadas.controllers.constantRegister.ConstantRegisterController
import com.axelros.constantesvitalesdigitalizadas.controllers.security.login.LoginController
import com.axelros.constantesvitalesdigitalizadas.controllers.patients.PatientsController
import com.axelros.constantesvitalesdigitalizadas.controllers.vitalSignMeasurementReport.VitalSignMeasurementReportController

class ControllersFactory {
    companion object: iControllersFactory {
        override fun createLoginController(
            view: LoginActivity,
            context: Context
        ):LoginController {
            return LoginController(view, context)
        }

        override fun createPatientsController(
            view: CensusActivity,
            context: Context
        ): PatientsController {
            return PatientsController(view,context)
        }
        override fun createConstantRegisterController(
            view: ConstantRegisterActivity,
            context: Context
        ): ConstantRegisterController {
            return ConstantRegisterController(view, context)
        }

        override fun createVitalSignsMeasurementReportController(
            view: VitalSignsMeasurementReportActivity,
            context: Context
        ): VitalSignMeasurementReportController {
            return VitalSignMeasurementReportController(view, context)
        }
    }
}
package com.axelros.constantesvitalesdigitalizadas.factory.measurementsFactory

import com.axelros.constantesvitalesdigitalizadas.ConstantRegisterActivity
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.model.DTO.MeasurementFieldDTO
import com.axelros.constantesvitalesdigitalizadas.model.MeasurementFields
import com.axelros.constantesvitalesdigitalizadas.model.VitalSign
import com.axelros.constantesvitalesdigitalizadas.model.VitalSignsList
import kotlinx.android.synthetic.main.activity_constant_register.*

class MeasurementsFactory {

    companion object: iMeasurementsFactory {
        var vitalSignsList: VitalSignsList? = null
        override fun createMeasurementsFields(view: ConstantRegisterActivity, vitalSignsList: VitalSignsList): MeasurementFields {
            Companion.vitalSignsList = vitalSignsList
            val measurementFields = MeasurementFields()
            //Pa Sistòlica
            measurementFields.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.presio_arterial),
                    view.getString(R.string.sistolica_nemotechnical),
                    view.etPaSistolica,
                    view.cbPaManual,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.presio_arterial)
                    )
                )
            )

            //Pa diastòlica
            measurementFields.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.presio_arterial),
                    view.getString(R.string.diastolica_nemotechnical),
                    view.etPaDiastolica,
                    view.cbPaManual,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.presio_arterial)
                    )
                )
            )

            //Pols
            measurementFields.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.pols),
                    view.getString(R.string.pols),
                    view.etPols,
                    view.cbPolsManual,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.pols)
                    )
                )
            )

            //T. Axil
            measurementFields.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Temperatura_Axilar),
                    view.getString(R.string.Temperatura_Axilar),
                    view.etTempAxilar,
                    null,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Temperatura_Axilar)
                    )
                )
            )
            //FR
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Frequencia_Respiratoria),
                    view.getString(R.string.Frequencia_Respiratoria),
                    view.etFreqResp,
                    null,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Frequencia_Respiratoria)
                    )
                )
            )
            //Eva
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Escala_Visual_Analogica),
                    view.getString(R.string.Escala_Visual_Analogica),
                    null,
                    null,
                    null,
                    null,
                    view.tvEva,
                    findVitalSignByName(
                        view.getString(R.string.Escala_Visual_Analogica)
                    )
                )
            )

            //Glicem
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Glicemia),
                    view.getString(R.string.Glicemia),
                    view.etGlicem,
                    null,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Glicemia)
                    )
                )
            )
            //Ins
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Insulina),
                    view.getString(R.string.Insulina),
                    view.etIns,
                    null,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Insulina)
                    )
                )
            )
            //Sat O2
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Saturacio_O2),
                    view.getString(R.string.Saturacio_O2),
                    view.etSatO2,
                    null,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Saturacio_O2)
                    )
                )
            )

            //Sat O2 amb O2
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Saturacio_O2_Amb_O2),
                    view.getString(R.string.Saturacio_O2_Amb_O2),
                    view.etSatO2Amb02,
                    null,
                    view.rgSatO2AmbO2Types,
                    view.spiO2AmbO2Value,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Saturacio_O2_Amb_O2)
                    )
                )
            )

            //Diuresi
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Diuresi),
                    view.getString(R.string.Diuresi),
                    view.etDiuresi,
                    null,
                    null,
                    null,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Diuresi)
                    )
                )
            )
            //Depos
            measurementFields?.addMeasurementField(
                MeasurementFieldDTO(
                    view.getString(R.string.Deposicions),
                    view.getString(R.string.Deposicions),
                    null,
                    null,
                    null,
                    view.spiDeposic,
                    null,
                    findVitalSignByName(
                        view.getString(R.string.Deposicions)
                    )
                )
            )
            return measurementFields
        }

        private fun findVitalSignByName(name:String):VitalSign?{
            vitalSignsList?.vitalSigns?.forEach{ vs ->
                if(vs.vsName.toUpperCase() == name.toUpperCase()) return vs
            }
            return null
        }


    }

}
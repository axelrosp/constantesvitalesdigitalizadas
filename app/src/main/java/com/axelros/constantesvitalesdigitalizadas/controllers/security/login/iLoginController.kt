package com.axelros.constantesvitalesdigitalizadas.controllers.security.login

interface iLoginController {
    fun login()
}
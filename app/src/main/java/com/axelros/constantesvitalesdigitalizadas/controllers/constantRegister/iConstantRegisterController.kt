package com.axelros.constantesvitalesdigitalizadas.controllers.constantRegister

import com.axelros.constantesvitalesdigitalizadas.model.CensusPatient
import com.axelros.constantesvitalesdigitalizadas.model.MeasurementFields

interface iConstantRegisterController{
    fun register(patient: CensusPatient, msrDateHourStart:String, measurementFields: MeasurementFields)
}
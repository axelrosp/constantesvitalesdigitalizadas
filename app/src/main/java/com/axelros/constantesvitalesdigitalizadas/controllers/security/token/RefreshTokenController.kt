package com.axelros.constantesvitalesdigitalizadas.controllers.security.token

import android.app.Activity
import android.content.Context
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.model.DTO.LoginUserDTO
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.google.gson.Gson
import java.util.HashMap


class RefreshTokenController {
    companion object {
        fun refreshAccesToken(context: Context, activity:Activity){
            val customSharedPreferences = CustomSharedPreferences(context)
            val jsonObjRequest = object : StringRequest(
                Request.Method.POST,
                NetworkPaths.API_URL_LOGIN, Response.Listener { response ->
                    Log.d("POST DONEEEEEEEEEEEE", response)
                    val gson = Gson()
                    val loginUserDTO = gson.fromJson(response, LoginUserDTO::class.java)

                    customSharedPreferences.save(Tags.LOGIN_USER_DTO.toString(), response)
                    customSharedPreferences.save(Tags.ACCES_TOKEN.toString(), loginUserDTO.access_token)
                    customSharedPreferences.save(Tags.REFRESH_TOKEN.toString(), loginUserDTO.refresh_token)

                },
                Response.ErrorListener {
                        error -> VolleyLog.d("POST ERROR", "Error: " + error.message)
                }) {

                override fun getBodyContentType(): String {
                    return "application/x-www-form-urlencoded; charset=UTF-8"
                }

                @Throws(AuthFailureError::class)
                override fun getParams(): Map<String, String> {
                    val params = HashMap<String, String>()
                    params["grant_type"] = "refresh_token"
                    params["refresh_token"] = customSharedPreferences.getValueString(Tags.REFRESH_TOKEN.toString())!!
                    return params
                }

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    val credentials = "constantesVitalesDigitalizadas" + ":" + "12345"
                    val headers = HashMap<String, String>()
                    headers["Content-Type"] = "application/x-www-form-urlencoded"
                    val auth = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
                    headers["Authorization"] = auth
                    return headers
                }

                override fun deliverError(error: VolleyError?) {
                    Toast.makeText(context, context.resources.getString(R.string.token_expired), Toast.LENGTH_LONG).show()
                    activity.finish()
                    activity.finishAffinity()
                }

            }

            VolleySingleton.getInstance(context).addToRequestQueue(jsonObjRequest)
        }


    }

}
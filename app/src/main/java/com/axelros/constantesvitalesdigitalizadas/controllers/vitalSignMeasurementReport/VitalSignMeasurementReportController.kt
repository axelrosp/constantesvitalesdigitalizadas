package com.axelros.constantesvitalesdigitalizadas.controllers.vitalSignMeasurementReport

import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.VitalSignsMeasurementReportActivity
import com.axelros.constantesvitalesdigitalizadas.network.volleyController.vitalSignMeasurement.VitalSignMeasurementController

class VitalSignMeasurementReportController(
    val view: VitalSignsMeasurementReportActivity,
    val context: Context
): iVitalSignMeasurementReportController {
    override fun getMeasurementReportOfPatient(cenId: String) {
        VitalSignMeasurementController.getMeasurementReportOfPatient(view, context, cenId)
    }
}
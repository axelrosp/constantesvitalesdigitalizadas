package com.axelros.constantesvitalesdigitalizadas.controllers.vitalSignMeasurementReport

interface iVitalSignMeasurementReportController {
    fun getMeasurementReportOfPatient(cenId: String)
}
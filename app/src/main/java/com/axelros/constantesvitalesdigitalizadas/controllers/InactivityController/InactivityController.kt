package com.axelros.constantesvitalesdigitalizadas.controllers.InactivityController

import android.app.Activity
import android.app.ActivityManager
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.axelros.constantesvitalesdigitalizadas.LoginActivity
import com.axelros.constantesvitalesdigitalizadas.R
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.utils.TimeCustomUtils
import com.axelros.constantesvitalesdigitalizadas.utils.Timer.LogOutTimerTask
import java.util.*

class InactivityController(val customSharedPreferences: CustomSharedPreferences, val context: Context, val activity: Activity) {
    var timer:Timer? = null
    fun detectInactivityAndExecuteTimer(){
        Thread{(
                if(isApplicationBroughtToBackground() || isScreenLocked()) {
                    timer = Timer()
                    val logoutTimeTask = LogOutTimerTask(context)
                    timer!!.schedule(logoutTimeTask, TimeCustomUtils.TIME_END_SESSION_MILISECONDS)
                }
                )}.start()
    }

    fun isInactive():Boolean{
        if (timer != null) {
            timer!!.cancel()
            timer!!.purge()
            timer = null
            if(customSharedPreferences?.getValueInt(Tags.END_SESSION.toString()) == 1) {
                Toast.makeText(context, context.getString(R.string.end_session), Toast.LENGTH_LONG).show()
                logout()
                return true
            }
        }
        return false
    }

    private fun isScreenLocked():Boolean {
        Thread.sleep(1000)
        val myKM = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        return myKM.isDeviceLocked

    }

    private fun isApplicationBroughtToBackground(): Boolean {
        val am = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val tasks = am.getRunningTasks(1)
        if (!tasks.isEmpty()) {
            val topActivity = tasks[0].topActivity
            if (topActivity.packageName != context.packageName) {
                return true
            }
        }
        return false
        }
    private fun logout(){
        Thread {
            val i = Intent(context, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(i)
            customSharedPreferences!!.clearSharedPreference()
            activity.finish()
        }.start()


    }

}
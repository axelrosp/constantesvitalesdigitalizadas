package com.axelros.constantesvitalesdigitalizadas.controllers.constantRegister

import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.ConstantRegisterActivity
import com.axelros.constantesvitalesdigitalizadas.model.CensusPatient
import com.axelros.constantesvitalesdigitalizadas.model.MeasurementFields
import com.axelros.constantesvitalesdigitalizadas.network.volleyController.measurement.MeasurementController

class ConstantRegisterController(val view: ConstantRegisterActivity, val context: Context): iConstantRegisterController {
    override fun register(patient: CensusPatient, msrDateHourStart:String, measurementFields:MeasurementFields ) {
        MeasurementController.postMeasurementHTTPVolley(view, context, patient, msrDateHourStart, measurementFields!!)
    }


}
package com.axelros.constantesvitalesdigitalizadas.controllers.security.login

import android.content.Context
import android.content.Intent
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyLog
import com.android.volley.toolbox.StringRequest
import com.axelros.constantesvitalesdigitalizadas.LoginActivity
import com.axelros.constantesvitalesdigitalizadas.CensusActivity
import com.axelros.constantesvitalesdigitalizadas.enums.Tags
import com.axelros.constantesvitalesdigitalizadas.model.DTO.LoginUserDTO
import com.axelros.constantesvitalesdigitalizadas.network.NetworkPaths
import com.axelros.constantesvitalesdigitalizadas.network.VolleySingleton
import com.axelros.constantesvitalesdigitalizadas.network.volleyController.user.UserController
import com.axelros.constantesvitalesdigitalizadas.network.volleyErrorHelper.VolleyErrorHelper
import com.axelros.constantesvitalesdigitalizadas.sharedpreferences.CustomSharedPreferences
import com.axelros.constantesvitalesdigitalizadas.utils.CustomProgressDialog
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import java.util.HashMap
import com.android.volley.VolleyError
import com.axelros.constantesvitalesdigitalizadas.R


class LoginController(val view: LoginActivity, val context: Context): iLoginController {
    override fun login(){
        val customProgressDialog = CustomProgressDialog(context, view, R.layout.progress_loading)
        val customSharedPreferences = CustomSharedPreferences(context)

        customProgressDialog.setDialog(true)
        val jsonObjRequest = object : StringRequest(
            Request.Method.POST,
            NetworkPaths.API_URL_LOGIN, Response.Listener { response ->
                Log.d("POST DONE", "$response")
                val gson = Gson()
                val loginUserDTO = gson.fromJson(response, LoginUserDTO::class.java)


                customSharedPreferences.save(Tags.LOGIN_USER_DTO.toString(), response)
                customSharedPreferences.save(Tags.ACCES_TOKEN.toString(), loginUserDTO.access_token)
                customSharedPreferences.save(Tags.REFRESH_TOKEN.toString(), loginUserDTO.refresh_token)

                var floor = (view.spiFloor?.selectedItemId!! + 1).toInt()
                customSharedPreferences.save(Tags.FLOOR.toString(), floor)

                UserController.getUserByUsername(
                    loginUserDTO.username,
                    context,
                    view
                )
                customProgressDialog.setDialog(false)
                val intent = Intent(context, CensusActivity::class.java)
                context.startActivity(intent)

            },
            Response.ErrorListener {
                    error -> VolleyLog.d("POST ERROR", "Error: " + error.message)
            }) {

            override fun getBodyContentType(): String {
                return "application/x-www-form-urlencoded; charset=UTF-8"
            }

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["username"] = view.etUsername.text.toString()
                params["password"] = view.etPassword.text.toString()
                params["grant_type"] = "password"
                return params
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val credentials = "constantesVitalesDigitalizadas" + ":" + "12345"
                val headers = HashMap<String, String>()
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                val auth = "Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
                headers["Authorization"] = auth
                return headers
            }

            override fun deliverError(error: VolleyError?) {
                customProgressDialog.setDialog(false)
                val errorMsg = VolleyErrorHelper.getMessage(error!!, context, view)
                Toast.makeText(context, errorMsg, Toast.LENGTH_LONG).show()
                if(errorMsg == context.resources.getString(R.string.invalid_username_password)){
                    view.etUsername.error = context.resources.getString(R.string.invalid_username_password)
                    view.etPassword.text.clear()
                    view.etUsername.requestFocus()
                }
            }

        }

        VolleySingleton.getInstance(context).addToRequestQueue(jsonObjRequest)
    }

}
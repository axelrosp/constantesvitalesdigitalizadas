package com.axelros.constantesvitalesdigitalizadas.controllers.patients

import android.content.Context
import com.axelros.constantesvitalesdigitalizadas.CensusActivity
import com.axelros.constantesvitalesdigitalizadas.network.volleyController.census.CensusController

class PatientsController(val view: CensusActivity, val context: Context): iPatientsController {
    override fun drawPatients(){
        CensusController.fillCensusPatientsListByFloor(view, context)
    }
}